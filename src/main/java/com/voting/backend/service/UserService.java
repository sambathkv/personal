package com.voting.backend.service;

import com.voting.backend.model.User;
import com.voting.backend.repo.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  @Autowired UserRepository userRepository;

  /**
   * User signup.
   *
   * @param user user
   * @return User
   */
  public User add(User user) {
    return userRepository.save(user);
  }
}
