package com.voting.backend.controller;

import com.voting.backend.model.User;
import com.voting.backend.service.UserService;
import com.voting.backend.util.AppResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/club-voting")
public class UserController {
  @Autowired UserService userService;

  @RequestMapping("/user/signup")
  public ResponseEntity<AppResponse<User>> add(@Validated @RequestBody User user) {
    try {
      User businessUser = userService.add(user);
      return ResponseEntity.ok(
          AppResponse.<User>builder()
              .success(true)
              .data(businessUser)
              .message("User sign up successful.")
              .build());
    } catch (Exception exception) {
      return ResponseEntity.status(HttpStatus.CONFLICT)
          .body(
              AppResponse.<User>builder()
                  .success(false)
                  .data(null)
                  .message(exception.getMessage())
                  .build());
    }
  }
}
