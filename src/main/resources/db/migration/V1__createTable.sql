CREATE TABLE IF NOT EXISTS users (
  id int(20) NOT NULL AUTO_INCREMENT,
  email varchar(100) NOT NULL UNIQUE,
  firstname varchar(100) NOT NULL,
  secondname varchar(100) NOT NULL,
  password varchar(100) NOT NULL,
  mobile varchar(20) NOT NULL UNIQUE,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;